syn keyword tfsmStatement	Value
syn keyword tfsmFunction 	Start


hi def link tfsmStatement	Define
hi def link tfsmFunction    Function
